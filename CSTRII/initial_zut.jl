# This Julia file generates initial guess values in the proper order
# based on the fitting results
#
# Oswaldo
# Sep 2021

zfit = readdlm("states_fit.txt",'\t')
ufit = readdlm("control_fit.txt",'\t')
time = readdlm("time.txt",'\t')

z1guess = Array{Float64}(undef,N,M)
z2guess = Array{Float64}(undef,N,M)
z3guess = Array{Float64}(undef,N,M)
z4guess = Array{Float64}(undef,N,M)
u1guess = Array{Float64}(undef,N,M)
u2guess = Array{Float64}(undef,N,M)
tguess = Array{Float64}(undef,N,M)

for i in ni,j in mj
    z1guess[i,j] = zfit[j+M*(i-1),1]
    z2guess[i,j] = zfit[j+M*(i-1),2]
    z3guess[i,j] = zfit[j+M*(i-1),3]
    z4guess[i,j] = zfit[j+M*(i-1),4]
    if ufit[j+M*(i-1),1] < u1L
        u1guess[i,j] = u1L
    elseif ufit[j+M*(i-1),1] > u1U
        u1guess[i,j] = u1U
    else
        u1guess[i,j] = ufit[j+M*(i-1),1]
    end
    if ufit[j+M*(i-1),2] < u2L
        u2guess[i,j] = u2L
    elseif ufit[j+M*(i-1),2] > u2U
        u2guess[i,j] = u2U
    else
        u2guess[i,j] = ufit[j+M*(i-1),2]
    end
    
    tguess[i,j] = time[j+M*(i-1),1]
end