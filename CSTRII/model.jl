# This Julia file contains the CSTR oprimal control model by applying
# a switched system formulation
#
# Oswaldo
# oandresm@uwaterloo.ca
# Nov 2021
m = Model(Ipopt.Optimizer)

# Variables
@variable(m,z1[i in ni,j in mj],start = z1guess[i,j])
@variable(m,z2[i in ni,j in mj],start = z2guess[i,j])
@variable(m,z3[i in ni,j in mj],start = z3guess[i,j])
@variable(m,z4[i in ni,j in mj],start = z4guess[i,j])
@variable(m,u1L <= w1[i in ni,j in mj] <= u1U,start=u1guess[i,j])     
@variable(m,u2L <= w2[i in ni,j in mj] <= u2U,start=u2guess[i,j])   
@variable(m,-1<=y1[p in np,i in ni]<=1,start=y10[p,i])
@variable(m,-1<=y2[p in np,i in ni]<=1,start=y20[p,i])  
@variable(m,tt[i in ni,j in mj] >=0,start=tguess[i,j])
@variable(m,v[i in ni]>=1e-3,start = 0.02) 

# ====================================  Auxiliary expressions ================================
# - Penalty function
function γp(x)
    return log(0.5,x)
 end
 register(m,:γp,1,γp,autodiff=true)
 
 # Expressions for spline interpolation
@NLexpression(m,Δu1p[i in ni],w1[i,M]-w1[i,x])
@NLexpression(m,Δu10[i in ni],w1[i,x]-w1[i,1])
@NLexpression(m,Δτp,τ[end]-τ[x])
@NLexpression(m,Δτ0,τ[x]-0)
@NLexpression(m,C1p[i in ni],3*(Δu1p[i]/Δτp - Δu10[i]/Δτ0)/(2*(Δτ0+Δτp)))
@NLexpression(m,b10[i in ni],Δu10[i]/Δτ0 - Δτ0*C1p[i]/3)
@NLexpression(m,b1p[i in ni],Δu1p[i]/Δτp - 2*Δτp*C1p[i]/3)
@NLexpression(m,d10[i in ni],C1p[i]/(3*Δτ0))
@NLexpression(m,d1p[i in ni],-C1p[i]/(3*Δτp))

@NLexpression(m,Δu2p[i in ni],w2[i,M]-w2[i,x])
@NLexpression(m,Δu20[i in ni],w2[i,x]-w2[i,1])
@NLexpression(m,C2p[i in ni],3*(Δu2p[i]/Δτp - Δu20[i]/Δτ0)/(2*(Δτ0+Δτp)))
@NLexpression(m,b20[i in ni],Δu20[i]/Δτ0 - Δτ0*C2p[i]/3)
@NLexpression(m,b2p[i in ni],Δu2p[i]/Δτp - 2*Δτp*C2p[i]/3)
@NLexpression(m,d20[i in ni],C2p[i]/(3*Δτ0))
@NLexpression(m,d2p[i in ni],-C2p[i]/(3*Δτp))

 # Expression for using 
@NLexpression(m,u1[i in ni,j in mj],
         ((y1[1,i]+1)/2)*u1L + ((y1[2,i]+1)/2)*u1U + ((y1[3,i]+1)/2)*w1[i,j])
@NLexpression(m,u2[i in ni,j in mj],
         ((y2[1,i]+1)/2)*u2L + ((y2[2,i]+1)/2)*u2U + ((y2[3,i]+1)/2)*w2[i,j])

# f expressions         
@NLexpression(m,f1[i in ni,j in mj2],1.5e7*(0.5251-z1[i,j])*exp(-10/(z2[i,j]+0.6932))
                  - 1.5e10*(0.4748+z1[i,j])*exp(-15/(z2[i,j]+0.6932)) - 1.4280)

@NLexpression(m,f2[i in ni,j in mj2],1.5e7*(0.4263-z2[i,j])*exp(-10/(z4[i,j]+0.6560))
                  - 1.5e10*(0.5764+z3[i,j])*exp(-15/(z4[i,j]+0.6560)) - 0.5086)         

# - RHS of the state equations 
@NLexpression(m,g1[i in ni,j in mj2],-3*z1[i,j] + f1[i,j])

@NLexpression(m,g2[i in ni,j in mj2],
            -11.1558*z2[i,j] + f1[i,j] - 8.1558*(z2[i,j]+0.1592)*u1[i,j])

@NLexpression(m,g3[i in ni,j in mj2],1.5*(0.5*z1[i,j]-z3[i,j])+f2[i,j])

@NLexpression(m,g4[i in ni,j in mj2],
            0.75*z2[i,j] - 4.9385*z4[i,j] + f2[i,j] - 3.4385*(z4[i,j]+0.122)*u2[i,j])

# - Differential equations in the domain s
@NLexpression(m,z1dot[i in ni,j in mj2],v[i]*g1[i,j])
@NLexpression(m,z2dot[i in ni,j in mj2],v[i]*g2[i,j])
@NLexpression(m,z3dot[i in ni,j in mj2],v[i]*g3[i,j])
@NLexpression(m,z4dot[i in ni,j in mj2],v[i]*g4[i,j])

#   Equation for t(s)
@NLexpression(m,tdot[i in ni,j in mj2],v[i])

# - Penalty terms
@NLexpression(m,Jp1[i in ni,j in mj],sum(δ*γp(y1[p,i]^2) for p in np))
@NLexpression(m,Jpint1[i in ni],sum(ω[j-1]*v[i]*Jp1[i,j] for j in mj2))
@NLexpression(m,Jpen1,sum(Jpint1[i] for i in ni))

@NLexpression(m,Jp2[i in ni,j in mj],sum(δ*γp(y2[p,i]^2) for p in np))
@NLexpression(m,Jpint2[i in ni],sum(ω[j-1]*v[i]*Jp2[i,j] for j in mj2))
@NLexpression(m,Jpen2,sum(Jpint2[i] for i in ni))

# =============================================================================================

# Objective function
@NLobjective(m,Min,z1[N,M]^2 + z2[N,M]^2 + z3[N,M]^2 + z4[N,M]^2 + Jpen1 + Jpen2)


# Constraints
# - collocation equations
@NLconstraint(m,h1[i in ni,j in mj2],sum(Ω[j,k]*z1[i,k] for k in mk) == z1dot[i,j])
@NLconstraint(m,h2[i in ni,j in mj2],sum(Ω[j,k]*z2[i,k] for k in mk) == z2dot[i,j])
@NLconstraint(m,h3[i in ni,j in mj2],sum(Ω[j,k]*z3[i,k] for k in mk) == z3dot[i,j])
@NLconstraint(m,h4[i in ni,j in mj2],sum(Ω[j,k]*z4[i,k] for k in mk) == z4dot[i,j])
@NLconstraint(m,h10[i in ni,j in mj2],sum(Ω[j,k]*tt[i,k] for k in mk) == tdot[i,j])

# - continuity
@NLconstraint(m,c1[i in 2:N],z1[i,1] == z1[i-1,M])
@NLconstraint(m,c2[i in 2:N],z2[i,1] == z2[i-1,M])
@NLconstraint(m,c3[i in 2:N],z3[i,1] == z3[i-1,M])
@NLconstraint(m,c4[i in 2:N],z4[i,1] == z4[i-1,M])
@NLconstraint(m,ct[i in 2:N],tt[i,1] == tt[i-1,M])      

# - low order representation of u(t) along the singular arc
@NLconstraint(m,u1sa[i in ni,j in 1:(x-1)],w1[i,j] ==
                   w1[i,1] + b10[i]*(τ[j]-0) + d10[i]*(τ[j]-0)^3)

@NLconstraint(m,u1sb[i in ni,j in (x+1):(M-1)],w1[i,j] == w1[i,x] 
          + b1p[i]*(τ[j]-τ[x]) + C1p[i]*(τ[j]-τ[x])^2 + d1p[i]*(τ[j]-τ[x])^3)

@NLconstraint(m,u2sa[i in ni,j in 1:(x-1)],w2[i,j] ==
                   w2[i,1] + b20[i]*(τ[j]-0) + d20[i]*(τ[j]-0)^3)

@NLconstraint(m,u2sb[i in ni,j in (x+1):(M-1)],w2[i,j] == w2[i,x] 
          + b2p[i]*(τ[j]-τ[x]) + C2p[i]*(τ[j]-τ[x])^2 + d2p[i]*(τ[j]-τ[x])^3)

#@NLconstraint(m,u2c[i in ni,j in mj;j!=2],w2[i,j] == w2[i,2])

# Boundary values 
@NLconstraint(m,z1iv,z1[1,1] == z1in)
@NLconstraint(m,z2iv,z2[1,1] == z2in)
@NLconstraint(m,z3iv,z3[1,1] == z3in)         
@NLconstraint(m,z4iv,z4[1,1] == z4in)
@NLconstraint(m,ttiv,tt[1,1] == 0)
@NLconstraint(m,ttfv,tt[N,M] == tf)       

# - Assignment constraints
@NLconstraint(m,ass1[i in ni],sum(y1[p,i] for p in np) == 2 - P)
@NLconstraint(m,ass2[i in ni],sum(y2[p,i] for p in np) == 2 - P)