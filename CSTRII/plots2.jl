#  This Julia file generates the plots with the results obtained
#   by executing the main file

close("all")

# values of u within null subintervals (v ~ 0) are removed for plotting
for i in ni
    if i == 1
       continue
    end
    if vs[i] < 1e-6
       u1s[((i-1)*M+1):i*M] .= u1s[(i-1)*M]
       u2s[((i-1)*M+1):i*M] .= u2s[(i-1)*M]
    end
 end


figure()
plot(tts,[z1s z2s z3s z4s],linewidth=2)
xlabel(L"t",fontsize=14)
legend([L"z_1",L"z_2",L"z_3",L"z_4"],fontsize=12)
ax = gca()
setp(ax[:get_yticklabels](),fontsize=12)
setp(ax[:get_xticklabels](),fontsize=12)
tight_layout()
savefig("states.pdf")

figure()
plot(tts,[u1s u2s],linewidth=2)
xlabel(L"t",fontsize=14)
legend([L"u_1",L"u_2"],fontsize=12)
ax = gca()
setp(ax[:get_yticklabels](),fontsize=12)
setp(ax[:get_xticklabels](),fontsize=12)
tight_layout()
savefig("controls.pdf")