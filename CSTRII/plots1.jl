#  This Julia file generates the plots with the results obtained
#   by executing the main file

using PyPlot

close("all")
# Time vector
t = Array{Float64}(undef,N*M)
for i in 1:N
	for j in 1:M
      if i == 1
         tpre = 0
      else
         tpre = t[M*(i-1)]
      end
      t[j+M*(i-1)] = τ[j]*h[i] + tpre
	end
end


# S domain 
for i in 2:N
   Ss[(i-1)*M+(i-1)] = t[(i-1)*M]
end
for i in ni
   for j in mj
      Ss[(M+1)*(i-1)+j] = t[M*(i-1)+j]
   end
end
pushfirst!(Ss,0.0)

figure()
plot(Ss,[z1s z2s z3s z4s],linewidth=2)
xlabel(L"s",fontsize=14)
legend([L"z_1",L"z_2",L"z_3",L"z_4"],fontsize=12)
ax = gca()
setp(ax[:get_yticklabels](),fontsize=12)
setp(ax[:get_xticklabels](),fontsize=12)
savefig("statesS.pdf")

figure()
plot(Ss,[u1s u2s],linewidth=2)
xlabel(L"s",fontsize=14)
legend([L"u_1",L"u_2"],fontsize=12)
ax = gca()
setp(ax[:get_yticklabels](),fontsize=12)
setp(ax[:get_xticklabels](),fontsize=12)
savefig("controlsS.pdf")

