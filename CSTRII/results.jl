# Auxiliary file to read the optimal result from the solution
z1s = transpose(value.(z1).data)[:]
z2s = transpose(value.(z2).data)[:]
z3s = transpose(value.(z3).data)[:]
z4s = transpose(value.(z4).data)[:]
u1s  = transpose(value.(u1).data)[:]
u2s  = transpose(value.(u2).data)[:]
w1s  = transpose(value.(w1).data)[:]
w2s  = transpose(value.(w2).data)[:]
vs   = value.(v).data
tts  = transpose(value.(tt).data)[:]
Tsw = Array{Float64}(undef,N-1)
for i in 1:N-1
    Tsw[i] = value(tt[i,M])
end
Jpen1s = value(Jpen1)
Jpen2s = value(Jpen2)
println("Jpen1: ",Jpen1s)
println("Jpen2: ",Jpen2s)
println("Switching times : ")
using Printf
for i in 1:N-1
    @printf("t%i: %.3f\n",i,Tsw[i])
end