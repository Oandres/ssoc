# This julia file generates initial guess values for z,u,X and y based on
# the sequence Seq0 

Seq1i = Dict{Int64,Int64}()
for i in ni
   Seq1i[i] = Seq10[i]
end

Seq2i = Dict{Int64,Int64}()
for i in ni
   Seq2i[i] = Seq20[i]
end

Seq1j = sort(collect(Seq1i),by=x->x[1])
Seq2j = sort(collect(Seq2i),by=x->x[1])

y10 = Array{Float64}(undef,P,N)
y10 .= -1.0
for i in ni
   k = Seq1j[i][2]
   y10[k,i] = 1.0
end  

y20 = Array{Float64}(undef,P,N)
y20 .= -1.0
for i in ni
   k = Seq2j[i][2]
   y20[k,i] = 1.0
end 

