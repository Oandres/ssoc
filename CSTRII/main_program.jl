# This Julia file solves the CSTR oprimal control model by applying
# a switched system formulation
#
# Oswaldo
# oandresm@uwaterloo.ca
# Nov 2021

using JuMP, Ipopt, PyPlot

# Scalars
# - Initial values
z1in = 0.1962  # Initial value for z1
z2in = -0.0372 # Initial value for z2
z3in = 0.0946  # Initial value for z3
z4in = 0       # Initial value for z4
tf   = 0.32498 # Final time
# - Bounds on u1 and u2
u1L  = -1      # Lower bound for u1
u1U  = 1       # Upper bound for u1
u2L  = -1      # Lower bound for u2
u2U  = 1       # Upper bound for u2

#Size parameters
N = 5  # Number of subintervals
M = 9   # Number of collocation points plus the beginning 
P = 3   # Number of subsystems
x = 5   # Inner point for spline interp

# Collocation matrix and points and quadrature weights
# (Radau points)
include("matrix.jl")
Ω,Ψ,τ = colmatrix(M,"Radau")

include("Rtableau.jl")
A,ω,c = butcher(M)

# Sets
np = 1:P  
ni = 1:N
mj = 1:M      # Collocation points plus the beginning point
mj2 = 2:M     # Collocation points only
mk = copy(mj)

δ = 1e1 # Penalty parameter

# Initial guess 
# - Guessed sequence
Seq10 = [1 1 3 3 2]
Seq20 = [1 1 1 2 2]
include("initial_guess.jl")
include("initfit.jl")
include("initial_zut.jl")

# Generate the nlp 
include("model.jl")

# Solution
optimize!(m)

# Reading results
include("results.jl")
# Plots in the domain s
#include("plots1.jl")
# Plots in the domain t
include("plots2.jl")

