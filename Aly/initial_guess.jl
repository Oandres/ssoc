# This julia file generates initial guess values for z,u,X and y based on
# the sequence Seq0 

Seqi = Dict{Int64,Int64}()
for i in ni
   Seqi[i] = Seq0[i]
end

Seqj = sort(collect(Seqi),by=x->x[1])

y0 = Array{Float64}(undef,P,N)
y0 .= -1.0
for i in ni
   k = Seqj[i][2]
   y0[k,i] = 1.0
end  

