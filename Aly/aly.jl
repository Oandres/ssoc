# This Julia file contains the Aly optimal control problem
# The problem is formulated as a switched system 
#
# Orthogonal collocation is applied on each interval
#
# Oswaldo
# Nov 2021

# Model definition
m = Model(Ipopt.Optimizer)

# Variables
@variable(m,z1[i in ni,j in mj],start = z1in)
@variable(m,z2[i in ni,j in mj],start = z2in)
@variable(m,z3[i in ni,j in mj],start = z3in)
#@variable(m,uL + ϵ <= w[i in ni,j in mj] <= uU - ϵ,start = uU/2)
@variable(m,uL <= w[i in ni,j in mj] <= uU,start = uU/2)
@variable(m,-1<=y[p in np,i in ni]<=1,start=y0[p,i])
@variable(m,tt[i in ni,j in mj]>=0)
@variable(m,v[i in ni]>=1e-7,start=1.41)

# ================  Auxiliary expressions ============
# - Penalty function
function γp(x)
   return log(0.5,x)
end
register(m,:γp,1,γp,autodiff=true)

# Expressions for spline interpolation
@NLexpression(m,Δup[i in ni],w[i,M]-w[i,x])
@NLexpression(m,Δu0[i in ni],w[i,x]-w[i,1])
@NLexpression(m,Δτp,τ[end]-τ[x])
@NLexpression(m,Δτ0,τ[x]-0)
@NLexpression(m,Cp[i in ni],3*(Δup[i]/Δτp - Δu0[i]/Δτ0)/(2*(Δτ0+Δτp)))
@NLexpression(m,b0[i in ni],Δu0[i]/Δτ0 - Δτ0*Cp[i]/3)
@NLexpression(m,bp[i in ni],Δup[i]/Δτp - 2*Δτp*Cp[i]/3)
@NLexpression(m,d0[i in ni],Cp[i]/(3*Δτ0))
@NLexpression(m,dp[i in ni],-Cp[i]/(3*Δτp))


@NLexpression(m,u[i in ni,j in mj],
         ((y[1,i]+1)/2)*uL + ((y[2,i]+1)/2)*uU + ((y[3,i]+1)/2)*w[i,j])


# - RHS of the state equations
@NLexpression(m,g1[i in ni,j in mj2],z2[i,j])
@NLexpression(m,g2[i in ni,j in mj2],u[i,j])
@NLexpression(m,g3[i in ni,j in mj2],(1/2)*(z1[i,j]^2+z2[i,j]^2))

# - Collocation equations
#   RHS of the state equations in the domain s
@NLexpression(m,z1dot[i in ni,j in mj2],v[i]*g1[i,j])
@NLexpression(m,z2dot[i in ni,j in mj2],v[i]*g2[i,j])
@NLexpression(m,z3dot[i in ni,j in mj2],v[i]*g3[i,j])

#   Equation for t(s)
@NLexpression(m,tdot[i in ni,j in mj2],v[i])

# - Penalty term
@NLexpression(m,Jp[i in ni,j in mj],sum(δ*γp(y[p,i]^2) for p in np))
@NLexpression(m,Jpint[i in ni],sum(ω[j-1]*v[i]*Jp[i,j] for j in mj2))
@NLexpression(m,Jpen,sum(Jpint[i] for i in ni))


# Objective function
@NLobjective(m,Min,z3[N,M] + Jpen)

# Constraints
# - RK representation of the state and adjoint equations
@NLconstraint(m,h1[i in ni,j in mj2],sum(Ω[j,k]*z1[i,k] for k in mk) == z1dot[i,j])
@NLconstraint(m,h2[i in ni,j in mj2],sum(Ω[j,k]*z2[i,k] for k in mk) == z2dot[i,j])
@NLconstraint(m,h3[i in ni,j in mj2],sum(Ω[j,k]*z3[i,k] for k in mk) == z3dot[i,j])
@NLconstraint(m,h4[i in ni,j in mj2],sum(Ω[j,k]*tt[i,k] for k in mk) == tdot[i,j])

# - Continuity
@NLconstraint(m,c1[i in 2:N],z1[i,1] == z1[i-1,M])
@NLconstraint(m,c2[i in 2:N],z2[i,1] == z2[i-1,M])
@NLconstraint(m,c3[i in 2:N],z3[i,1] == z3[i-1,M])
@NLconstraint(m,ct[i in 2:N],tt[i,1] == tt[i-1,M])

# - Low order representation of u(t) for the singular arcs
# @NLconstraint(m,gu[i in ni,j in 2:(M-1)],w[i,j] == w[i,1]
#                                                   + (w[i,M]-w[i,1])*τ[j])

@NLconstraint(m,g9a[i in ni,j in 2:(x-1)],w[i,j] ==
                  w[i,1] + b0[i]*(τ[j]-0) + d0[i]*(τ[j]-0)^3)

@NLconstraint(m,g9b[i in ni,j in (x+1):(M-1)],w[i,j] == w[i,x] 
         + bp[i]*(τ[j]-τ[x]) + Cp[i]*(τ[j]-τ[x])^2 + dp[i]*(τ[j]-τ[x])^3)


# Boundary values 
@NLconstraint(m,z1iv,z1[1,1] == z1in)
@NLconstraint(m,z2iv,z2[1,1] == z2in)
@NLconstraint(m,z3iv,z3[1,1] == z3in)
@NLconstraint(m,ttiv,tt[1,1] == 0)
@NLconstraint(m,ttfv,tt[N,M] == tf)

# - Assignment constraints
@NLconstraint(m,ass[i in ni],sum(y[p,i] for p in np) == 2 - P)


