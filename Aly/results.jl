# Auxiliary file to read the optimal result from the solution
z1s = transpose(value.(z1).data)[:]
z2s = transpose(value.(z2).data)[:]
z3s = transpose(value.(z3).data)[:]
us  = transpose(value.(u).data)[:]
ws  = transpose(value.(w).data)[:]
vs   = value.(v).data
tts  = transpose(value.(tt).data)[:]
Jpens = value(Jpen)
println("Jpen: ",Jpens)
Tsw = Array{Float64}(undef,N-1)
for i in 1:N-1
    Tsw[i] = value(tt[i,M])
end
println("Switching times : ")
using Printf
for i in 1:N-1
    @printf("t%i: %.3f\n",i,Tsw[i])
end