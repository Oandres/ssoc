# This Julia file contains Aly optimal control problem
# The problem is formulated as a switched system 
#
# Oswaldo Andres-Martinez
oswxandres@gmail.com
# July 2021
# Update: November 2021

using JuMP, Ipopt, PyPlot 

# Scalars
z1in = 0  
z2in = 1  
z3in = 0   
tf  = 5
# - Bounds on u
uL   = -1  
uU   = 1   

# Size parameters
N = 3  # Number of finite elements
M = 9   # Number of collocation points plus the beginning 
P = 3   # Number of subsystems
x = 5   # Inner point for spline interp

# Collocation matrix and points and quadrature weights
# (Radau points)
include("matrix.jl")
Ω,Ψ,τ = colmatrix(M,"Radau")
# Radau tableu
include("Rtableau.jl")
A,ω,r = butcher(M)

# Sets
np = 1:P  
ni = 1:N
mj = 1:M  # Collocation points plus the beginning point
mj2 = 2:M  # Collocation points only
mk = copy(mj)

δ = 1e2 # Penalty parameter
#ϵ = 1e-7

# Initial guess 
# - Guessed sequence
Seq0 = [1 1 3]
include("initial_guess.jl")

# Generate the nlp 
include("aly.jl")

# Solution
JuMP.optimize!(m)

# Reading results
include("results.jl")
# Plots in the domain s
include("plots1.jl")
# Plots in the domain t
include("plots2.jl")
