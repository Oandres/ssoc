# Auxiliary file for orthogonal collocation approach
#
# This file generates the Collocation matrix A for orthogonal collocation
# based on jacobi polynomials (Legendre, Radau or Lobatto) and the 
# vector lag with the Lagrange polynomial coefficients for interpolation.
# 
# Oswaldo A.
# Jan 2018
using Jacobi

# =================== Collocation matrices ================
function colmatrix(M,poly)
   n = M-2; # Number of internal collocation points
   zc = Array{Float64}(undef,n+2,1);
   zc[1] = -1;
   zc[end] = 1;
   if poly == "Legendre"
      zc[2:end-1] = jacobi_zeros(n,0,0);  # Legendre
   elseif poly == "Radau"
      zc[2:end-1] = jacobi_zeros(n,1,0); # Radau
   elseif poly == "Lobbato"
      zc[2:end-1] = jacobi_zeros(n,1,1); # Lobbato
   else
      error("poly must have one of these: Radau,Legendre, Lobbato")
   end

   # In the interval [a,b]
   a = 0; b = 1;
   zc = ((b-a)/2)*zc .+ (b+a)/2;

   # Collocation matrix 
   Q = ones(n+2,n+2);
   R = zeros(n+2,n+2);
   for k in 1:(n+1)
	   Q[:,k+1] = zc.^k;
	   R[:,k+1] = k*zc.^(k-1);
   end
   A = R/Q
   return A,Q,zc
end

