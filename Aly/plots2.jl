#  This Julia file generates the plots with the results obtained
#   by executing the main file

figure()
plot(tts,[z1s z2s z3s us],linewidth=2)
xlabel(L"t",fontsize=14)
legend([L"z_1",L"z_2",L"z_3",L"u"],fontsize=12)
ax = gca()
setp(ax[:get_yticklabels](),fontsize=12)
setp(ax[:get_xticklabels](),fontsize=12)
#savefig("statesT.pdf")
tight_layout()
savefig("aly.pdf")

# figure()
# plot(tts,us,linewidth=2)
# xlabel(L"t",fontsize=14)
# legend([L"u"],fontsize=12)
# ax = gca()
# setp(ax[:get_yticklabels](),fontsize=12)
# setp(ax[:get_xticklabels](),fontsize=12)
# savefig("controlT.pdf")

