#  This Julia file generates the plots with the results obtained
#   by executing the main file

using PyPlot

close("all")
# Time vector
t = Array{Float64}(undef,N*M)
for i in 1:N
	for j in 1:M
      if i == 1
         tpre = 0
      else
         tpre = t[M*(i-1)]
      end
      t[j+M*(i-1)] = τ[j] + tpre
	end
end

figure()
plot(t,[z1s z2s z3s],linewidth=2)
xlabel(L"s",fontsize=14)
legend([L"z_1",L"z_2",L"z_3"],fontsize=12)
ax = gca()
setp(ax[:get_yticklabels](),fontsize=12)
setp(ax[:get_xticklabels](),fontsize=12)
savefig("statesS.pdf")

figure()
plot(t,us,linewidth=2)
xlabel(L"s",fontsize=14)
legend([L"u"],fontsize=12)
ax = gca()
setp(ax[:get_yticklabels](),fontsize=12)
setp(ax[:get_xticklabels](),fontsize=12)
savefig("controlS.pdf")

