# SSOC

This folder contains the Julia files that belong to the Supplementary Material of
the paper:

"A switched system approach for the direct solution of singular optimal control problems"
- Oswaldo Andrés-Martínez, Luis A. Ricardez-Sandoval

